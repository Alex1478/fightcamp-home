//
//  Fonts.swift
//  fightcamp-home
//
//  Created by Alexandre Marcotte on 6/13/19.
//  Copyright © 2019 FightCamp. All rights reserved.
//

import UIKit

extension UIFont {
    
    // font names
    private static var boldFontName  : String { return "ProximaNova-Bold"  }
    private static var lightFontName : String { return "ProximaNova-Light" }
    
    // header
    open class var header : UIFont {
        
        return UIFont(name: boldFontName, size: 18)!
    }
    
    // title
    open class var title : UIFont {
        
        return UIFont(name: boldFontName, size: 14)!
    }
    
    // subtitle
    open class var subtitle : UIFont {
        
        return UIFont(name: lightFontName, size: 14)!
    }
}
