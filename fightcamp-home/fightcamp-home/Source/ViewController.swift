//
//  ViewController.swift
//  fightcamp-home
//
//  Created by Alexandre Marcotte on 6/13/19.
//  Copyright © 2019 FightCamp. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        
        let testLabel = UILabel()
        
        testLabel.font = .header
        testLabel.text = "Hello FightCamp"
        
        view.addSubview(testLabel)
        
        testLabel.sizeToFit()
        
        testLabel.center = CGPoint(x: view.bounds.midX, y: view.bounds.midY)
    }
}

