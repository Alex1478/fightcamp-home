![](./img/mockup.png)


## Description

The candidate will design a single view iOS application.

The app is a simplified version of a home screen to help the user selecting his next workout from different categories (all workouts, or filtered by rounds).

## Workouts home screen breakdown

- Workout cell
- Workouts categories
- Workouts categories table

--- 

### 1. Workout cell

Collection cell containing specific information about the workout.

The metadata to include is: 

    1. Thumbnail picture
    2. Workout title
    3. Trainer's nickname 
    
        * If no trainer's nickname, use his first and last name

![](./img/cell.png)

---

### 2. Workouts categories

Horizontal scrolling layout that contains multiple workout cells
    
![](./img/category.png)

There is 4 different categories:

    1. Recently added workouts (all workouts)
    2. only 4 rounds workouts
    3. only 8 rounds workouts
    4. only 10 rounds workouts

---

### 3. Workouts categories table
    
Vertical scrolling table to browse between the multiple workouts categories
    
Include a header/title section for each category
    
![](./img/table.png)

## Basic API

Use the FightCamp REST api in order to populate the home screen. The `Workouts` and `Trainers` public endpoints will be used.

### 1. Workouts data

`https://dev-app.fightcamp.io/api/workouts?status=live&type=classic`

If needed, append the query parameter `nbr_rounds` to fetch the workouts data filtered by the number of rounds. 

Also, `limit` and `offset` query parameters can be used for paging purpose.

**Note**: for the purpose of this test, the query parameters `status=live` and `type=classic` should be used.

### 2. Trainers data

`https://dev-app.fightcamp.io/api/trainers`

## Requirements

- The UI must be entirely programmatic; do not use Storyboards or Nibs to create the UI
- The user must be noticed when the data is loading by using an activity indicator (spinner) or any other types of loading state UI element
- Use `Fonts.swift` (included in the base project) to customize the labels font
- The code must compile for iOS 12
- Avoid third-party dependencies for the purpose of this test
    - no Swift PM, CocoaPod or Carthage
- There is no need to build an iPad layout

## Criterias

The candidate will be evaluated according to several criterias: 

- The UI is similar to the mockup proposed in this document.        

    Don't worry too much about the paddings and the size of the cells. 
    
    Also, the content in the cells will vary from the proposed mockup.

- Clarity of the code 
    - Easy to read
    - Consistent naming convention betweens files, classes, functions etc. 
- Good use of the Swift language features
    - e.g. Using Decodable protocol
    - foreach, map, filter, Enums etc.
- The UI is fluid enough (no performance issue)

### Bonus

For the purpose of this test, this is completely fine to load and store all the workouts at the start up, then filtering them afterward to populate the UI.

However, loading the workouts dynamically as the user is scrolling through the collection is definitely a plus


## Steps

1. Clone the `fightcamp-home` repository

`https://bitbucket.org/Alex1478/fightcamp-home/`

2. Implement the home screen
3. Share the project to a.marcotte@hykso.com
    * You can simply compress/zip the project
        
        or
        
    * Host the project to any git repository and send an invitation